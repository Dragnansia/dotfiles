# Waydroid openSUSE Tumbleweed
### Initialisation
[link reference](https://www.reddit.com/r/openSUSE/comments/xjxs29/comment/ipep2th/)

#### Waydroid dependencies
repo:
[https://download.opensuse.org/repositories/home:/xanders/openSUSE_Tumbleweed/](https://download.opensuse.org/repositories/home:/xanders/openSUSE_Tumbleweed/)

```sh
zypper ar -f https://download.opensuse.org/repositories/home:/xanders/openSUSE_Tumbleweed/ home:xanders:waydroid
```

#### Kernel dependencies
repo: [https://download.opensuse.org/repositories/home:/xanders:/branches:/openSUSE:/Factory-vfio/standard/](https://download.opensuse.org/repositories/home:/xanders:/branches:/openSUSE:/Factory-vfio/standard/)

```sh
zypper ar -f https://download.opensuse.org/repositories/home:/xanders:/branches:/openSUSE:/Factory-vfio/standard/ 
```

#### Packages
```sh
zypper in libgbinder1 libglibutil1 python38-gbinder waydroid xclip wl-clipboard
```

### Change Kernel
Add kernel version from `home:xanders`
![Change kernel image](./images/change_kernel.png)

Restart your computer after this change.

### Init Waydroid
```sh
waydroid init
```

### dbus policy trust
Create dbus config file `/etc/dbus-1/system.d/system-local.conf` with this configuration:  
```xml
<!DOCTYPE busconfig PUBLIC "-//freedesktop//DTD D-Bus Bus Configuration 1.0//EN"
    "http://www.freedesktop.org/standards/dbus/1.0/busconfig.dtd">
<busconfig>
	<policy context="default">
		<allow own="id.waydro.Container" />
		<allow send_destination="id.waydro.Container" />
		<allow user="root" />
	</policy>
</busconfig>
```

### Enable waydroid container service
```sh
systemctl enable --now waydroid-container.service
```

### Network
Verify with `ip addr` that you have a bridge with this name `waydroid0`  
If you don't have one launch:
```sh
/usr/lib/waydroid/data/scripts/waydroid-net.sh
```

Open YaST firewall and add `waydroid0` on `Interfaces` with `trusted` group.

### Start waydroid with full ui
```sh
waydroid show-full-ui
```

