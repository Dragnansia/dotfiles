FOLDER_PATH=$(dirname "$(realpath "$0")")

# Install nvim config and packer dependencie
if [[ ! -d "$HOME/.config/nvim/" ]]; then
    ln -s "$FOLDER_PATH/config/nvim" "$HOME/.config/nvim"
fi

# Install neovide config
if [[ ! -d "$HOME/.config/neovide/" ]]; then
    ln -s "$FOLDER_PATH/config/neovide" "$HOME/.config/neovide"
fi

if [[ ! -d "$HOME/.local/share/nvim/site/pack/packer/start/packer.nvim/" ]]; then
    git clone --depth 1 https://github.com/wbthomason/packer.nvim "$HOME/.local/share/nvim/site/pack/packer/start/packer.nvim"
fi

# git config
git config --global core.editor nvim
git config --global commit.gpgsign true

if [[ ! $(git config --global user.signingkey) ]]; then
    echo "Need to configure signing key"
fi
