vim.g.mapleader = " "
vim.keymap.set("n", "<leader>pv", vim.cmd.Ex)

-- Copy and past
vim.keymap.set("n", "<c-c>", '"+y')
vim.keymap.set("v", "<c-c>", '"+y')
vim.keymap.set("n", "<c-v>", '"+p')
vim.keymap.set("i", "<c-v> <c-r>", "+")
vim.keymap.set("c", "<c-v> <c-r>", "+")
vim.keymap.set("i", "<c-r> <c-v>", "")

-- FTerm
-- local toggle_term = function()
-- 	require("FTerm").toggle()
-- end
-- vim.keymap.set("n", "<leader>t", toggle_term)
-- vim.keymap.set("t", "<ESC>", toggle_term)

-- Floaterm
vim.keymap.set("n", "<leader>t", ":FloatermToggle<CR>")
vim.keymap.set("t", "<ESC>", "<Cmd>FloatermHide<CR>")
vim.keymap.set("t", "<F10>", "<Cmd>FloatermPrev<CR>")
vim.keymap.set("t", "<F12>", "<Cmd>FloatermNext<CR>")
vim.keymap.set("t", "<F2>", "<Cmd>FloatermNew<CR>")
vim.keymap.set("t", "<F1>", "<Cmd>FloatermKill<CR>")

-- Telescope
vim.keymap.set("n", "<leader>f", ":Telescope find_files<CR>")
vim.keymap.set("n", "<leader>b", ":Telescope buffers<CR>")
vim.keymap.set("n", "<leader>r", ":Telescope lsp_references<CR>")

-- Cmp nvim
vim.keymap.set("n", "K", ":lua vim.lsp.buf.hover()<CR>")
vim.keymap.set("n", "ga", ":lua vim.lsp.buf.code_action()<CR>")
vim.keymap.set("n", "gi", ":lua vim.lsp.buf.implementation()<CR>")
vim.keymap.set("n", "gd", ":lua vim.lsp.buf.definition()<CR>")
vim.keymap.set("n", "gI", ":lua vim.diagnostic.open_float()<CR>")
vim.keymap.set("n", "<leader>r", ":lua vim.lsp.buf.rename()<CR>")

-- Mason
vim.keymap.set("n", "<leader>m", ":Mason<CR>")

-- Debugging
vim.keymap.set("n", "<leader>db", function()
	require("dap").toggle_breakpoint()
end)
vim.keymap.set("n", "<leader>dB", function()
	local condition = vim.fn.input("Breakpoint condition: ")
	require("dap").set_breakpoint(condition)
end)
vim.keymap.set("n", "<leader>dg", function()
	require("dap").goto_()
end)
vim.keymap.set("n", "<leader>dG", function()
	local line = vim.fn.input("Goto line number: ")
	require("dap").goto_(line)
end)
vim.keymap.set("n", "<leader>dc", function()
	require("dap").continue()
end)
vim.keymap.set("n", "<leader>dn", function()
	require("dap").step_over()
end)
vim.keymap.set("n", "<leader>do", function()
	require("dap").step_out()
end)
vim.keymap.set("n", "<leader>di", function()
	require("dap").step_into()
end)
vim.keymap.set("n", "<leader>du", function()
	require("dapui").toggle()
end)
