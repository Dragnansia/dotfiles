return require("packer").startup(function(use)
	-- packer
	use("wbthomason/packer.nvim")

	use({ "nvim-treesitter/nvim-treesitter", run = ":TSUpdate" })

	-- Debug
	use("theHamsta/nvim-dap-virtual-text")
	use({ "rcarriga/nvim-dap-ui", requires = { "mfussenegger/nvim-dap" } })
	use("jay-babu/mason-nvim-dap.nvim")
	use("folke/neodev.nvim")

	-- Colorshemes
	use("EdenEast/nightfox.nvim")
	use("arcticicestudio/nord-vim")
	use("luisiacc/gruvbox-baby")
	use("ellisonleao/gruvbox.nvim")
	use("nyoom-engineering/oxocarbon.nvim")

	-- Others
	use("lukas-reineke/indent-blankline.nvim")
	use("tami5/lspsaga.nvim")
	use("jiangmiao/auto-pairs")
	use("mhartington/formatter.nvim")
	use("saecki/crates.nvim")

	use("nvim-lua/plenary.nvim")
	use("nvim-lua/popup.nvim")
	use("nvim-telescope/telescope-media-files.nvim")
	use("nvim-telescope/telescope.nvim")
	use("stevearc/dressing.nvim")
	use("rcarriga/nvim-notify")
	use("stevearc/overseer.nvim")

	use("jose-elias-alvarez/null-ls.nvim")
	use("lewis6991/hover.nvim")
	use({
		"nvim-lualine/lualine.nvim",
		requires = { "kyazdani42/nvim-web-devicons" },
	})

	use("https://gitlab.com/gabmus/vim-blueprint")

	-- use("numToStr/FTerm.nvim")
	use("voldikss/vim-floaterm")

	-- Git
	use("lewis6991/gitsigns.nvim")

	-- Comment
	use({
		"numToStr/Comment.nvim",
		config = function()
			require("Comment").setup()
		end,
	})

	-- LSP
	use({
		"VonHeikemen/lsp-zero.nvim",
		requires = {
			-- LSP Support
			{ "neovim/nvim-lspconfig" },
			{ "williamboman/mason.nvim" },
			{ "williamboman/mason-lspconfig.nvim" },

			-- Autocompletion
			{ "hrsh7th/nvim-cmp" },
			{ "hrsh7th/cmp-buffer" },
			{ "hrsh7th/cmp-path" },
			{ "saadparwaiz1/cmp_luasnip" },
			{ "hrsh7th/cmp-nvim-lsp" },
			{ "hrsh7th/cmp-nvim-lua" },

			-- Snippets
			{ "L3MON4D3/LuaSnip" },
			{ "rafamadriz/friendly-snippets" },
		},
	})

	use("WhoIsSethDaniel/mason-tool-installer.nvim")

	-- Startup
	use({
		"goolord/alpha-nvim",
		requires = { "nvim-tree/nvim-web-devicons" },
		config = function()
			require("alpha").setup(require("alpha.themes.dashboard").config)
		end,
	})
end)
