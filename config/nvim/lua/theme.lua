vim.o.background = "dark"

-- gruvbox-baby
vim.g.gruvbox_baby_telescope_theme = 1
vim.g.hruvbox_baby_transparent_mode = 1

vim.cmd("colorscheme gruvbox-baby")
-- vim.cmd("colorscheme nightfox")
-- vim.cmd("colorscheme gruvbox")
-- vim.cmd("colorscheme oxocarbon")
