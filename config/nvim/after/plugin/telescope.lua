local telescope = require("telescope")

telescope.load_extension("media_files")

telescope.setup({
	pickers = {
		buffers = {
			theme = "dropdown",
		},
	},
	defaults = {
		borderchars = {
			prompt = { "─", " ", " ", " ", "─", "─", " ", " " },
			results = { " " },
			preview = { " " },
		},
	},
})

require("overseer").setup()
