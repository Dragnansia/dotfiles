-- require("FTerm").setup({
-- 	border = "double",
-- 	dimensions = {
-- 		height = 0.9,
-- 		width = 0.9,
-- 	},
-- })

vim.g.floaterm_width = 0.9
vim.g.floaterm_height = 0.9
vim.g.floaterm_borderchars = "═║═║╔╗╝╚"
