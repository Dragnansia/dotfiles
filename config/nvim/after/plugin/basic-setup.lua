local null_ls = require("null-ls")

null_ls.setup({
	source = {
		null_ls.builtins.formatting.stylua,
		null_ls.builtins.diagnostics.eslint,
		null_ls.builtins.completion.spell,
	},
})

require("Comment").setup()

-- local alpha_theme = require("alpha.themes.dashboard")
-- require("alpha").setup(alpha_theme.config)
