local lsp = require("lsp-zero")

lsp.preset("recommended")
lsp.nvim_workspace()
lsp.setup()

-- need to call mason setup before mason-nvim-dap
require("mason").setup()

local mason_dap = require("mason-nvim-dap")
mason_dap.setup({
	-- default DAP
	ensure_installed = { "codelldb" },
	automatic_setup = true,
})

-- default formatter and lsp i want
require("mason-tool-installer").setup({
	ensure_installed = {
		"rust-analyzer",
		"stylua",
		"lua-language-server",
		"tailwindcss-language-server",
	},
})

local dap = require("dap")

dap.adapters.codelldb = {
	type = "server",
	port = "${port}",
	executable = {
		command = vim.fn.stdpath("data") .. "/mason/bin/codelldb",
		args = { "--port", "${port}" },
	},
}

dap.configurations.rust = {
	{
		type = "codelldb",
		request = "launch",
		name = "Launch file",
		program = function()
			local path = vim.fn.getcwd() .. "/file"
			return vim.fn.input("Path to executable (Hello World): " .. path)
		end,
		args = function()
			local arguments = {}
			local argsInput = vim.fn.input("Arguments: ")
			-- Need to be more specifique
			local _ = string.gsub(argsInput, "([^ ]+)", function(c)
				arguments[#arguments + 1] = c
			end)
			return arguments
		end,
		cwd = "${workspaceFolder}",
	},
}

require("nvim-dap-virtual-text").setup({})
