vim.g.neovide_transparency = 0.9
vim.g.neovide_remember_window_size = true
vim.g.neovide_hide_mouse_when_typing = true
vim.g.neovide_cursor_vfx_mode = "pixiedust"
vim.g.neovide_refresh_rate = 144

vim.opt.guifont = { "MonoLisa", "h18" }
